# Pistol Whip Randomizer

_Replaces original songs with matching ID songs randomly from CustomSongs folder so you don't have to mess with filenames yourself._

![Pistol Whip Randomizer](/PWR.png)

Author: rava.games (Discord: `@rava#4045`)

Requirements: Python 3

Usage: See script comments
