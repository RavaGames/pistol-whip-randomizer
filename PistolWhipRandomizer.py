import glob
import random
from shutil import copyfile

# HOWTO:
# 0) Create CustomSongs folder in
#    ...\Pistol Whip\Pistol Whip_Data\StreamingAssets\Audio\GeneratedSoundBanks\Windows

# 1) Place all custom .wem files in the CustomSongs folder
#    File names should be in "Song_Name_12345678.wem" format,
#    where the numbers correspond to existing file names

# 2) Place this script in the CustomSongs folder

# 3) Back up original songs if you wish to, the script will DESTROY them

# 4) Run the script
# 5) The script will index all .wem files in the original folder,
#    and pick a replacecement file with a matching ID randomly

originalSongs = glob.glob("../*.wem")
customSongs = glob.glob("./*.wem")

print("\n".join(customSongs))

# Parse all original wem file names so custom tracks can be compared to them
# This includes all of the sound effects that reside in the folder,
# but they'll just be skipped if no replacements were found
for i in range(len(originalSongs)):
    originalSongs[i] = originalSongs[i].replace("..\\", "").split(".")[0]

# Filter songs that have replacements available based on IDs parsed from file names
songsWithReplacements = []

for original in originalSongs:
    matchingTracks = glob.glob("./*" + original + ".wem")

    tracks = len(matchingTracks)
    
    if (tracks > 0):
        songsWithReplacements.append(original)
        print("\n[%s] has %d replacements available" % (original, tracks))
        print("\t"+"\n\t".join(matchingTracks))

print("\n\nRandomizing...\n\n")

# Pick random songs and replace originals
for song in songsWithReplacements:
    replacementSongs = glob.glob("./*" + song + ".wem")
    customSong = random.choice(replacementSongs)
    customSong = customSong.replace("\\", "/")    
    copyfile(customSong, "../%s.wem" % song)

    print("Replaced ../%s with %s" % (song, customSong))

